# Originally based on conkyrc found at:
# http://mylinuxramblings.wordpress.com/2010/03/23/how-to-configure-the-conky-system-monitor/

background yes

# Create own window instead of using desktop (required in nautilus)
own_window yes
own_window_type desktop
#own_window_type override
own_window_hints below,undecorated,sticky,skip_taskbar,skip_pager

# Use double buffering (reduces flicker, may not work for everyone)
double_buffer yes

# fiddle with window
use_spacer yes
use_xft no

# Update interval in seconds
update_interval 2.0

#Maximum Width of Window
maximum_width 320

# Minimum size of text area
# minimum_size 250 5

# Draw shades?
draw_shades no

# Text stuff
draw_outline no # amplifies text if yes
draw_borders no
font arial
uppercase no # set to yes if you want all text to be in uppercase

# Stippled borders?
stippled_borders 3

# border margins
border_margin 5

# border width
border_width 0

# Default colors and also border colors, grey90 == #e5e5e5
default_color FFFFCC

own_window_colour brown
own_window_transparent yes

# Text alignment, other possible values are commented
alignment top_left
#alignment top_right
#alignment bottom_left
#alignment bottom_right

# Gap between borders of screen and text
gap_x 10
gap_y 10

# stuff after ‘TEXT’ will be formatted on screen

TEXT
$color
${color CC9900}SYSTEM ${hr 2}$color
Host:$alignr$nodename
System: $alignr$sysname $kernel $machine
Uptime:$alignr$uptime
${color CC9900}CPU ${hr 2}$color
${execi 1000 cat /proc/cpuinfo | grep 'model name' | sed -e 's/model name.*: //'| uniq}
Total CPU: ${cpu cpu0}%
${color 597DB2}${cpubar}$color
${cpugraph 000000 597DB2 .5 -t -l}
Core 0: ${freq_g 1} GHz        $alignr Temperature: $color ${exec sensors|grep 'Core 0'|awk '{print $3}'|cut -c1-5}C
${cpu cpu1}% ${color 597DB2}${cpubar cpu1}$color
Core 1: ${freq_g 2} GHz        $alignr Temperature: $color ${exec sensors|grep 'Core 1'|awk '{print $3}'|cut -c1-5}C
${cpu cpu2}% ${color 597DB2}${cpubar cpu2}$color

NAME            PID	 CPU%  	MEM%
${color CCFFFF}${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
${top name 5} ${top pid 5} ${top cpu 5} ${top mem 5}$color

${color CC9900}MEMORY ${hr 2}$color
RAM Used: ${mem}	${alignr}RAM Free: ${memfree}/${memmax}
RAM:  $memperc%  ${alignr 220}${color FF6600}${membar 6}$color
Swap: $swapperc%   ${alignr 220}${color FF6600}${swapbar 6}$color

${color CC9900}DISK ${hr 2}$color
sda2 ${fs_type /} (Root):  ${fs_free_perc /}% ${color FFFF33}${fs_bar 6 /}$color
sda6 ${fs_type /home} (Home):    ${fs_free_perc /home}% ${color FFFF33}${fs_bar 6 /home}$color
sda7 NTFS (Data):    ${fs_free_perc /mnt/Data}% ${color FFFF33}${fs_bar 6 /mnt/Data}$color

${color CC9900}NETWORK ${hr 2}$color
Public IP: $alignr${execi 600 wget http://checkip.dyndns.org/ -q -O - | grep -Eo '\<[[:digit:]]{1,3}(\.[[:digit:]]{1,3}){3}\>'}
${if_match "${addr eth0}"!="No Address"}${color CC9900}${hr 1}$color
${color CC9900}eth0:$color${alignr}${addr eth0}
Down: $color${downspeed eth0} k/s ${alignr}Up: ${upspeed eth0} k/s
${downspeedgraph eth0 25,140 000000 ff0000} ${alignr}${upspeedgraph eth0 25,140 000000 00ff00}$color
Total: ${totaldown eth0} ${alignr}Total: ${totalup eth0}
${endif}${if_match "${addr wlan0}"!="No Address"}${color CC9900}${hr 1}$color
${color CC9900}wlan0:$color${alignr}${addr wlan0}
Down: $color${downspeed wlan0} k/s ${alignr}Up: ${upspeed wlan0} k/s
${downspeedgraph wlan0 25,140 000000 ff0000 .5 -t -l} ${alignr}${upspeedgraph wlan0 25,140 000000 00ff00 .5 -t -l}$color
Total: ${totaldown wlan0} ${alignr}Total: ${totalup wlan0}
${endif}${if_existing /sys/class/net/usb0}${color CC9900}${hr 1}$color
${color CC9900}USB Tether:$color${alignr}${addr usb0}
Down: $color${downspeed usb0} k/s ${alignr}Up: ${upspeed usb0} k/s
${downspeedgraph usb0 25,140 000000 ff0000 .5 -t -l} ${alignr}${upspeedgraph usb0 25,140 000000 00ff00 .5 -t -l}$color
Total: ${totaldown usb0} ${alignr}Total: ${totalup usb0}
${endif}
${color CC9900}Outbound Connections${hr 1}$color
${tcp_portmon 1 65535 rhost 0}:${tcp_portmon 1 65535 rport 0}
${tcp_portmon 1 65535 rhost 1}:${tcp_portmon 1 65535 rport 1}
${tcp_portmon 1 65535 rhost 2}:${tcp_portmon 1 65535 rport 2}
${tcp_portmon 1 65535 rhost 3}:${tcp_portmon 1 65535 rport 3}
${tcp_portmon 1 65535 rhost 4}:${tcp_portmon 1 65535 rport 4}
${color CC9900}${font sans-serif:bold:size=8}SHORTCUT KEYS ${hr 2}$color
${font sans-serif:normal:size=8}Alt+F3$alignr Dmenu
Super+space$alignr Main Menu
Super+f$alignr File Manager
Super+e$alignr Editor
Super+m$alignr Media Player
Super+q$alignr Force Quit
Super+g$alignr Character Map
Super+l$alignr Lock
Super+r$alignr Read the DOC
Super+x$alignr Tiling Keybinds
